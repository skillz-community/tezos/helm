deployed=$(helm ls | grep -v "nginx" | tail -n+2 | cut -d' ' -f 1 | xargs)
for chain in $deployed;
do
    pod=$(kubectl get pods | grep $chain | cut -d' ' -f 1)
    kubectl logs $pod node | tail -n 1 | grep -v "Too few connection" > /dev/null
    res=$?
    if [ $res -eq 0 ]
    then
        echo "$chain syncing"
    else
        echo "$chain NOT syncing"
    fi
done