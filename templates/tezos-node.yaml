{{ $account := .Values.keys.import | ternary "my_account" "" }}
{{ $protocol := ternary "`cat /usr/local/share/tezos/active_protocol_versions | tail -n 1 | xargs`" .Values.protocol (kindIs "invalid" .Values.protocol) }}

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ .Release.Name }}-tezos
  labels:
    release: {{ .Release.Name }}
    component: tezos
spec:
  serviceName: {{ .Release.Name }}-tezos
  replicas: {{ .Values.deployment.replicas }}
  selector:
    matchLabels:
      release: {{ .Release.Name }}
      component: tezos
  
  volumeClaimTemplates:
  - metadata:
      name: node-volume
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: {{ .Values.deployment.storage.node }}

  - metadata:
      name: client-volume
    spec:
      accessModes: [ "ReadWriteMany" ]
      resources:
        requests:
          storage: 100Mi

  template:
    metadata:
      labels:
        release: {{ .Release.Name }}
        component: tezos
        info: tezos-pod

    spec:
      initContainers:
      {{ if .Values.keys.import }}
      - name: tezos-keys-unlocker
        image: {{ template "tezos.image" . }}
        imagePullPolicy: {{ .Values.imagePullPolicy }}
        {{ if .Values.keys.private }}
        command: ["/bin/sh", "-c", "tezos-client -d /var/run/tezos/client/ import secret key {{ $account }} {{ .Values.keys.private }} --force"]
        {{ else }}
        command: ["/bin/sh", "-c", "tezos-client -d /var/run/tezos/client/ add address {{ $account }} {{ .Values.keys.public }} --force"]
        {{ end }}
        volumeMounts:
          - name: client-volume
            mountPath: "/var/run/tezos/client/"
      {{ end }}

      - name: tezos-generate-node-identity
        image: {{ template "tezos.image" . }}
        imagePullPolicy: {{ .Values.imagePullPolicy }}
        command: ["/bin/sh", "-c", "tezos-node identity --data-dir=/var/run/tezos/node/data check || tezos-node identity --data-dir=/var/run/tezos/node/data generate"]
        volumeMounts:
          - name: node-volume
            mountPath: /var/run/tezos/node
        
      containers:
        - name: tezos-node
          image: {{ template "tezos.image" . }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          volumeMounts:
            - name: client-volume
              mountPath: /var/run/tezos/client
            - name: node-volume
              mountPath: /var/run/tezos/node
          command: ["/bin/sh", "-c", "entrypoint.sh tezos-node"]
          ports:
            - name: rpc
              containerPort: {{ .Values.service.ports.rpc }}
            - name: network
              containerPort: {{ .Values.service.ports.network }}

        {{ if .Values.options.baker.enable }}
        - name: tezos-baker
          image: {{ template "tezos.image" . }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          volumeMounts:
            - name: client-volume
              mountPath: /var/run/tezos/client
            - name: node-volume
              mountPath: /var/run/tezos/node
          command: ["/bin/sh", "-c", "PROTOCOL={{ $protocol }} entrypoint.sh {{ ternary "tezos-baker-test" "tezos-baker" .Values.test }} --max-priority 128 {{ $account }}"]
          env:
            - name: NODE_HOST
              value: "localhost"
        {{ end }}

        {{ if .Values.options.endorser.enable }}
        - name: tezos-endorser
          image: {{ template "tezos.image" . }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          command: ["/bin/sh", "-c", "PROTOCOL={{ $protocol }} entrypoint.sh {{ ternary "tezos-endorser-test" "tezos-endorser" .Values.test }} {{ $account }}"]
          env:
            - name: NODE_HOST
              value: "localhost"
        {{ end }}

        {{ if .Values.options.accuser.enable }}
        - name: tezos-accuser
          image: {{ template "tezos.image" . }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          command: ["/bin/sh", "-c", "PROTOCOL={{ $protocol }} entrypoint.sh {{ ternary "tezos-accuser-test" "tezos-accuser" .Values.test }} {{ $account }}"]
          env:
            - name: NODE_HOST
              value: "localhost"
        {{ end }}