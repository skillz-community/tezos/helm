# Tezos Helm Chart

[Tezos](https://tezos.com/) is an open-source platform for assets and applications backed by a global community of validators, researchers, and builders.

## Getting started

To install the helm chart on your cluster simply run:


```console
$ helm install <path_to_chart>
```

## Introduction

This chart deploys a **public** [Tezos](https://tezos.com/) network onto a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager. This network is connected to MainNet. This chart is comprised of 4 components:

## Prerequisites

* Kubernetes 1.8

## Installing the Chart

1. Install the chart as follows:

    ```console
    $ helm install my-release helm/tezos
        --set keys.import=true
        --set keys.public=[PUBLIC_ADDRESS]
        --set keys.private=[PRIVATE_KEY]
        --set options.baker=[true | false]
        --set options.endorser=[true | false]
        --set options.accuser=[true | false]
    ```

    using the above generated example, the configurations would equate to:
    
    * `keys.public` = `tz1P6zBcJjKxVVR2PXmstpnAJZkOj6avK4L6` 
    * `keys.private` = `38000e15ca0730Acc2d1b30faBaadbs93c45ea222a117e9e9c6l2a9872bb3bcf`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm uninstall my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

Please notre that the scaling of Tezos nodes involve a **StatefulSet** and it won't release PVC automatically.

## Configuration

The following table lists the configurable parameters of the tezos chart and their default values.

| Parameter                         | Description                                   | Default                               |
|-----------------------------------|-----------------------------------------------|---------------------------------------|
| `imagePullPolicy`                 | Container pull policy                         | `IfNotPresent`                        |
| `keys.import`                     | Wheter the public/private has to be imported  | `true`                                |
| `keys.public`                     | The public key to import                      |                                       |
| `keys.private`                    | The private key to import                     |                                       |
| `network`                         | The Tezos network to use                      | `carthagenet`                         |
| `protocol`                        | Set this value to force a protocol            |                                       |
| `ethstats.webSocketSecret`        | ethstats secret for posting data              | `my-secret-for-connecting-to-ethstats`|
| `ethstats.service.type`           | k8s service type for ethstats                 | `LoadBalancer`                        |
| `options.baker.enable`            | Whether the baker has to be deployed          | `false`                               |
| `options.endorser.enable`         | Whether the endorser has to be deployed       | `false`                               |
| `options.accuser.enable`          | Whether the accuser has to be deployed        | `false`                               |
| `deployment.replicas`             | The number of replicas for the tezos node     | `1`                                   |
| `deployment.image.repository`     | The repository name for the tezos             | `tezos/tezos`                         |
| `deployment.storage.node`         | The space to allocate per tezos node          | `50`                                  |


## Changelog

Please note that this helm chart use semantic versioning and did't hit the 1.0.0 so the API is not yet.

Take a look at our current issues to see what are our next milestones for this project.